module Actions
  module Chatbot

    class FetchFbUser
      extend LightService::Action

      expects :message_obj
      promises :fbuser_obj

      executed do |context|
        @context     = context
        @message_obj = context.message_obj
        context.fbuser_obj = fbuser_obj
      end

      def self.fbuser_obj
        graph = Koala::Facebook::API.new(ENV['FB_PAGE_ACCESS_TOKEN'])
        graph.get_object( @message_obj.sender.fetch("id"))
      end
    end

  end
end
