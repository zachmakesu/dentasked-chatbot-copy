module Actions
  module Chatbot

    class QuickReplyMessage
      extend LightService::Action

      expects :message_obj, :messenger_user
      promises :replies

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        context.replies = replies
      end

      def self.replies
        return [] unless @message_obj.messaging["message"]["quick_reply"]
        @messenger_user.update(message_concern: nil)
        payload = @message_obj.messaging["message"]["quick_reply"]["payload"]
        #PORO THAT RETURNS SPECIFIC ARRAYS
        ::Chatbot::PayloadHandler.response_from(message_obj: @message_obj, payload: payload, messenger_user: @messenger_user)
      end

    end

  end
end
