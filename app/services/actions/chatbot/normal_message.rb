module Actions
  module Chatbot

    class NormalMessage
      extend LightService::Action

      expects :message_obj, :messenger_user, :replies
      promises :replies

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        context.replies = replies
      end

      def self.replies
        return @context.replies unless @context.replies.empty?
        if @messenger_user.message_concern
          ::Chatbot::MessageConcernHandler.response_from(message_obj: @message_obj, messenger_user: @messenger_user)
        else
          ::Chatbot::Payloads::Random.replies(message_obj: @message_obj, messenger_user: @messenger_user)
        end
      end

    end

  end
end
