module Actions
  module Chatbot

    class DeleteMessageConcern
      extend LightService::Action

      expects :messenger_user

      executed do |context|
        @context          = context
        @messenger_user   = context.messenger_user
        delete_message_concern!
      end

      def self.delete_message_concern!
        @messenger_user.update(message_concern: nil)
      end

    end

  end
end
