class Chatbot::MessageConcerns::AskPhone
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    if message_obj.messaging["message"]["text"] =~ /^(?:Yes\b|No\b|yes\b|no\b)/
      messenger_user.update(message_concern: nil, sendable: false)
      [{ text: "Great! You can download Dentasked here https://dentasked.com/download to easily book your next booking appointment." }]
    else
      [{ text: "Yes or No only" }]
    end
  end
end
