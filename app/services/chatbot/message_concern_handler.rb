class Chatbot::MessageConcernHandler
  def self.response_from(message_obj:, messenger_user:)
    case messenger_user.message_concern
    when 'ASK_PHONE_CONCERN'            then Chatbot::MessageConcerns::AskPhone.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'CHECK_CONCERN'                then [ {text: "Nice test"} ]
    else
    []
    end
  end
end
