class Chatbot::Payloads::YesSubscribe
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "Thank you very much for subscribing!" }
    ]
  end
end
