class Chatbot::Payloads::OralHygieneFlossing
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "You can't reach the tight spaces between your teeth and under the gumline with a toothbrush. That's why daily flossing is important." },
      { text: "Don't skimp. Break off about 18 inches (46 centimeters) of dental floss. Wind most of the floss around the middle finger on one hand, and the rest around the middle finger on the other hand. Grip the floss tightly between your thumbs and forefingers." },
      { text: "Be gentle. Guide the floss between your teeth using a rubbing motion. Don't snap the floss into your gums. When the floss reaches your gumline, curve it against one tooth." },
      { text: "Take it one tooth at a time. Slide the floss into the space between your gum and tooth. Use the floss to gently rub the side of the tooth in an up-and-down motion. Unwind fresh floss as you progress to the rest of your teeth." },
      { text: "Keep it up. If you find it hard to handle floss, use an interdental cleaner — such as a dental pick, pre-threaded flosser, tiny brushes that reach between teeth, a water flosser or wooden or silicone plaque remover.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Back ⬅️",
            "payload": "ORAL_HYGIENE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Main ⬅️",
            "payload": "GET_STARTED_1_PAYLOAD"
          }
        ]
      }
    ]
  end
end
