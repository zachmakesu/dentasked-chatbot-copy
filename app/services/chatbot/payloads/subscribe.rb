class Chatbot::Payloads::Subscribe
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "Do you want to get the latest news, information, and offers from Dentasked?" },
      { text: "Dental implants are made of titanium, which is harder than steel.",
        quick_replies: [
          {
            content_type: "text",
            title: "Yes, absolutely!",
            payload: "SUBSCRIBE_YES_PAYLOAD"
          },
          {
            content_type: "text",
            title: "Maybe next time",
            payload: "SUBSCRIBE_NO_PAYLOAD"
          }
        ]
      }
    ]
  end
end
