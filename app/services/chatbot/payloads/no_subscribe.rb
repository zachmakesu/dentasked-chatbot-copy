class Chatbot::Payloads::NoSubscribe
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      {
        attachment: {
          type: "template",
          payload: {
            template_type: "button",
            text: "No hard feelings. We totally understand. If you change your mind next time, simply click on the button below, or the Subscribe button on the menu below.",
            buttons: [
              {
                type: "postback",
                title: "Subscribe",
                payload: "SUBSCRIBE_YES_PAYLOAD"
              }
            ]
          }
        }
      }
    ]
  end
end
