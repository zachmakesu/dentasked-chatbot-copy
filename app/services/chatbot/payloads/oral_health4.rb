class Chatbot::Payloads::OralHealth4
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "General Info",
                image_url: "https://imgur.com/KO6L82K.png",
              }
            ]
          }
        }
      },
      { text: "A 50:50 white vinegar-cold water solution makes an excellent denture soak." },
      { text: "A dry mouth harbors more cavity-causing bacteria than a wet mouth, so drink up." },
      { text: "A dry mouth harbors more halitosis (bad breath)-causing bacteria than a wet mouth, so drink up." },
      { text: "Acid weakens teeth by dissolving the calcium in our teeth." },
      { text: "Albert P. Southwick, a NY dentist in the late 1800s, invented the electric chair." },
      { text: "Dental implants are made of titanium, which is harder than steel.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Back ⬅️",
            "payload": "ORAL_HEALTH_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Main ⬅️",
            "payload": "GET_STARTED_1_PAYLOAD"
          }
        ]
      }
    ]
  end
end
