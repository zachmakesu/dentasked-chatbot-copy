class Chatbot::Payloads::OralHygieneRinsing
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "You can also include a dental rinse as part of your oral hygiene routine. A dental rinse is often recommended for additional oral hygiene if you wear braces." },
      { text: "Pour 20 milliliters of (4 teaspoons) of Listerine mouthwash into a cup." },
      { text: "Without diluting the solution with water, empty the cup into your mouth." },
      { text: "Rinse for a little longer, a full 30 seconds (try counting to 30 in your hear, or using a stopwatch). Don't worry if you can't get to 30 seconds the first time - it gets easier each time you try." },
      { text: "During rinsing gargle in your month." },
      { text: "Expel the solution in the sink.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Back ⬅️",
            "payload": "ORAL_HYGIENE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Main ⬅️",
            "payload": "GET_STARTED_1_PAYLOAD"
          }
        ]
      }
    ]
  end
end
