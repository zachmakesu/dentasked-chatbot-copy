class Chatbot::Payloads::OralHygieneBrushing
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "Oral health begins with clean teeth. Keeping the area where your teeth meet your gums clean can prevent gum disease, while keeping your tooth surfaces clean can help you stave off cavities." },
      { text: "Brush your teeth twice a day. When you brush, don't rush. Take time to do a thorough job." },
      { text: "Use the proper equipment. Use a fluoride toothpaste and a soft-bristled toothbrush that fits your mouth comfortably. Consider using an electric or battery-operated toothbrush, which can reduce plaque and a mild form of gum disease (gingivitis) more than does manual brushing. These devices are also helpful if you have arthritis or other problems that make it difficult to brush effectively." },
      { text: "Practice good technique. Hold your toothbrush at a slight angle — aiming the bristles toward the area where your tooth meets your gum. Gently brush with short back-and-forth motions. Remember to brush the outside, inside and chewing surfaces of your teeth, as well as your tongue." },
      { text: "Keep your equipment clean. Always rinse your toothbrush with water after brushing. Store your toothbrush in an upright position and allow it to air-dry until using it again. Try to keep it separate from other toothbrushes in the same holder to prevent cross-contamination. Don't routinely cover toothbrushes or store them in closed containers, which can encourage the growth of bacteria, mold and yeast." },
      { text: "Know when to replace your toothbrush. Invest in a new toothbrush or a replacement head for your electric or battery-operated toothbrush every three to four months — or sooner if the bristles become irregular or frayed.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Back ⬅️",
            "payload": "ORAL_HYGIENE_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Main ⬅️",
            "payload": "GET_STARTED_1_PAYLOAD"
          }
        ]
      }
    ]
  end
end
