class Chatbot::Payloads::OralHealth3
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "horizontal",
            elements: [
              {
                title: "Dental procedure 1",
                image_url: "https://imgur.com/ygSUPaE.png",
              },
              {
                title: "Dental procedure 2",
                image_url: "https://imgur.com/TBzNq63.png",
              }
            ]
          }
        }
      },
      { text: "1 in 7 people have impacted wisdom teeth." },
      { text: "1950's heart throb James Dean had no front teeth! He wore a bridge." },
      { text: "A dentist would rather save a diseased tooth than remove it." },
      { text: "A tooth that has been knocked out starts to die within 15 minutes, but if you put it in milk or hold it in your mouth it will survive longer. See a dentist ASAP!" },
      { text: "An impacted wisdom tooth is not removed by a dentist—an oral surgeon does!" },
      { text: "Dental implants are made of titanium, which is harder than steel.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Back ⬅️",
            "payload": "ORAL_HEALTH_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Main ⬅️",
            "payload": "GET_STARTED_1_PAYLOAD"
          }
        ]
      }
    ]
  end
end
