class Chatbot::Payloads::OralHealth
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "Hi #{messenger_user.first_name}, here are the Fun Facts about Oral Health" },
      {
        attachment: {
          type: "template",
          payload: {
            template_type: "list",
            top_element_style: "large",
            elements: [
              {
                title: "Brushing, Flossing, Rinsing",
                subtitle: "Lorem ipsum",
                image_url: "https://i.imgur.com/yAl7gMb.png",
                buttons: [
                  {
                    title: "Learn more",
                    type: 'postback',
                    payload: 'ORAL_HEALTH_1_PAYLOAD'
                  }
                ]
              },
              {
                title: "Dental cleaning",
                subtitle: "Lorem ipsum",
                image_url: "https://imgur.com/ygSUPaE.png",
                buttons: [
                  {
                    title: "Learn more",
                    type: 'postback',
                    payload: 'ORAL_HEALTH_2_PAYLOAD'
                  }
                ]
              },
              {
                title: "Dental surgeries and procedures",
                subtitle: "Lorem ipsum",
                image_url: "https://imgur.com/TBzNq63.png",
                buttons: [
                  {
                    title: "Learn more",
                    type: 'postback',
                    payload: 'ORAL_HEALTH_3_PAYLOAD'
                  }
                ]
              },
              {
                title: "General information",
                subtitle: "Lorem ipsum",
                image_url: "https://imgur.com/KO6L82K.png",
                buttons: [
                  {
                    title: "Learn more",
                    type: 'postback',
                    payload: 'ORAL_HEALTH_4_PAYLOAD'
                  }
                ]
              },
            ]
          }
        }
      }
    ]
  end
end
