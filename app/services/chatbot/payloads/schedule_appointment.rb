class Chatbot::Payloads::ScheduleAppointment
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true, message_concern: "ASK_PHONE_CONCERN")
    [
      { text: "Early diagnosis of tooth decay. The primary reason to visit your dentist regularly is to avoid dental issues such as tooth decay and gum problems. ... So, regular visits can help you in the diagnosis and treatment of these conditions. Clean teeth." },
      { text: "Do you have a Smart phone (Android/iOS)?" }
    ]
  end
end
