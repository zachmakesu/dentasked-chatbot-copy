class Chatbot::Payloads::UpcomingTrips


  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)

    upcoming = EventFilter.new(Event.not_deleted.upcoming, {}).result
    elements = upcoming.limit(9).decorate.map do |event|
      {
        title: event.name,
        subtitle: "Organizer: #{event.owner.full_name}
Date: #{event.travel_dates.upcase}
Description: #{event.description}
        ",
        image_url: event.cover_photo_complete_url(:large),
        buttons: [
          {
            type: "web_url",
            url: event.website,
            title: "View Details",
            webview_height_ratio: "tall"
          },
          {
            type: "web_url",
            url: event.generate_checkout_link,
            title: "Book Now",
            webview_height_ratio: "tall"
          }
        ]
      }
    end

    see_more = {
                  title: "See more Upcoming Trips",
                  buttons: [
                    {
                      type: "web_url",
                      url: "#{ActionController::Base.asset_host}/trips",
                      title: "See more",
                      webview_height_ratio: "tall"
                    }
                  ]
                }
    elements << see_more
    [
      { text: "Hey #{messenger_user.first_name}, here are the trips that you can join this upcoming weekend" },
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: elements
          }
        }
      }
    ]
  end
end
