class Chatbot::Payloads::OralHygiene
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Brushing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/yAl7gMb.png",
                buttons: [
                  {
                    type: "postback",
                    title: "Learn more",
                    payload: "ORAL_HYGIENE_BRUSHING_PAYLOAD"
                  }
                ]
              },
              {
                title: "Flossing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/mykJJda.png",
                buttons: [
                  {
                    type: "postback",
                    title: "Learn more",
                    payload: "ORAL_HYGIENE_FLOSSING_PAYLOAD"
                  }
                ]
              },
              {
                title: "Rinsing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/u2vRa4s.png",
                buttons: [
                  {
                    type: "postback",
                    title: "Learn more",
                    payload: "ORAL_HYGIENE_RINSING_PAYLOAD"
                  }
                ]
              }
            ]
          }
        }
      }
    ]
  end
end
