class Chatbot::Payloads::GetStarted
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "Hello #{messenger_user.first_name}, nice to have you here. How can I help you?" },
      { text: "Anyways, I'm new here. I can only do few things at a moment. Below are the things that I can offer to help you so far" },
      {
        attachment: {
          type: "template",
          payload: {
            template_type: "list",
            top_element_style: "large",
            elements: [
              {
                title: "Fun Facts about Oral Health",
                subtitle: "Lorem ipsum",
                image_url: "https://i.imgur.com/yAl7gMb.png",
                buttons: [
                  {
                    title: "Learn more",
                    type: 'postback',
                    payload: 'ORAL_HEALTH_PAYLOAD'
                  }
                ]
              },
              {
                title: "Tips For Best Oral Hygiene",
                subtitle: "Lorem ipsum",
                image_url: "https://imgur.com/nKXnkM2.png",
                buttons: [
                  {
                    title: "Learn more",
                    type: 'postback',
                    payload: 'ORAL_HYGIENE_PAYLOAD'
                  }
                ]
              },
              {
                title: "Schedule Your Next Dental Booking Appointment",
                subtitle: "Lorem ipsum",
                image_url: "https://imgur.com/KO6L82K.png",
                buttons: [
                  {
                    title: "Learn more",
                    type: 'postback',
                    payload: 'SCHEDULE_PAYLOAD'
                  }
                ]
              },
              {
                title: "Subscribe to Dentasked Messenger Notifications.",
                subtitle: "Lorem ipsum",
                image_url: "https://imgur.com/KO6L82K.png",
                buttons: [
                  {
                    title: "Check it!",
                    type: 'postback',
                    payload: 'SUBSCRIBE_PAYLOAD'
                  }
                ]
              },
            ]
          }
        }
      }
    ]
  end
end
