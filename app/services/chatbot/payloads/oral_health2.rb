class Chatbot::Payloads::OralHealth2
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: [
              {
                title: "Brushing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/nKXnkM2.png",
              },
              {
                title: "Flossing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/equUpC4.png",
              },
              {
                title: "Rinsing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/oWBIzCT.png",
              }
            ]
          }
        }
      },
      { text: "Dental diseases are the most common diseases worldwide, affecting 5 billion people. Don't be a statistic--have your teeth cleaned professionally." },
      { text: "300 types of bacteria make up dental plaque. Regular cleaning can remove stubborn and disgusting plaque!" },
      { text: "Bacteria in a dirty mouth can go to the lungs and cause lung infections. Keep your mouth clean with regular dental cleaning." },
      { text: "Black or green tea has antibacterial powers that help prevent cavities and gum disease—but they can stain your teeth. Lighten teeth satins with regular dental cleaning." },
      { text: "Cotton candy, which can cause tooth decay, was invented by Dr. William Morrison, a dentist. Fight cavities with regular dental cleaning.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Back ⬅️",
            "payload": "ORAL_HEALTH_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Main ⬅️",
            "payload": "GET_STARTED_1_PAYLOAD"
          }
        ]
      }
    ]
  end
end
