class Chatbot::Payloads::RecommendTrip
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "Hello #{messenger_user.first_name}, seems like you are having a hard time choosing which trips you should book." },
      { text: "Worry no more, I can help you with this. I will ask you few questions so that we can recommend the best trips for you." },
      {
        attachment:{
          type: "template",
          payload: {
            template_type: "button",
            text: "Just fill up the form and we're ready to go",
            buttons: [
              {
                title: "Fill up form",
                type: "web_url",
                url: "#{ActionController::Base.asset_host}/messenger_webview/fill_up_form",
                messenger_extensions: true,
                webview_height_ratio: "tall"
              }
            ]
          }
        }
      }
    ]
  end
end
