class Chatbot::Payloads::OralHealth1
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "horizontal",
            elements: [
              {
                title: "Brushing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/lKg2U2F.png",
              },
              {
                title: "Flossing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/B6OUtkf.png",
              },
              {
                title: "Rinsing",
                subtitle: "Lorem Ipsum",
                image_url: "https://imgur.com/h39cJ1T.png",
              }
            ]
          }
        }
      },
      { text: "Bacteria in the mouth can make its own acid from the food we eat. Remember to brush, rinse and floss!" },
      { text: "Before World War II, people did not brush their teeth every day." },
      { text: "Bleeding gums may be prevented by an antiseptic mouthwash used at least once a day." },
      { text: "Brushing your teeth is not enough—you should brush your tongue too, and floss every day!" },
      {
        text: "Children aged 3 to 6 years should use only a pea-sized amount of toothpaste.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Back ⬅️",
            "payload": "ORAL_HEALTH_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Main ⬅️",
            "payload": "GET_STARTED_1_PAYLOAD"
          }
        ]
      }
    ]
  end
end
