class Chatbot::PayloadHandler
  def self.response_from(message_obj:, payload:, messenger_user:)
    case payload
    when 'GET_STARTED_PAYLOAD'                then Chatbot::Payloads::GetStarted.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'GET_STARTED_1_PAYLOAD'              then Chatbot::Payloads::GetStarted1.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ORAL_HEALTH_PAYLOAD'                then Chatbot::Payloads::OralHealth.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'ORAL_HEALTH_1_PAYLOAD'                then Chatbot::Payloads::OralHealth1.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'ORAL_HEALTH_2_PAYLOAD'                then Chatbot::Payloads::OralHealth2.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'ORAL_HEALTH_3_PAYLOAD'                then Chatbot::Payloads::OralHealth3.replies(message_obj: message_obj, messenger_user: messenger_user)
      when 'ORAL_HEALTH_4_PAYLOAD'                then Chatbot::Payloads::OralHealth4.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'ORAL_HYGIENE_PAYLOAD'               then Chatbot::Payloads::OralHygiene.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ORAL_HYGIENE_BRUSHING_PAYLOAD'          then Chatbot::Payloads::OralHygieneBrushing.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ORAL_HYGIENE_FLOSSING_PAYLOAD'          then Chatbot::Payloads::OralHygieneFlossing.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ORAL_HYGIENE_RINSING_PAYLOAD'           then Chatbot::Payloads::OralHygieneRinsing.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'SCHEDULE_PAYLOAD'                   then Chatbot::Payloads::ScheduleAppointment.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'SUBSCRIBE_PAYLOAD'                  then Chatbot::Payloads::Subscribe.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'SUBSCRIBE_YES_PAYLOAD'                  then Chatbot::Payloads::YesSubscribe.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'SUBSCRIBE_NO_PAYLOAD'                  then Chatbot::Payloads::NoSubscribe.replies(message_obj: message_obj, messenger_user: messenger_user)
    # when 'UPCOMING_PAYLOAD'                   then Chatbot::Payloads::UpcomingTrips.replies(message_obj: message_obj, messenger_user: messenger_user)
    # when 'RECOMMEND_TRIP_PAYLOAD'             then Chatbot::Payloads::RecommendTrip.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'CHECK_PAYLOAD' then [ {text: "Nice test"} ]
    else

    # require "pry"
    # binding.pry

    []
    end
  end
end
