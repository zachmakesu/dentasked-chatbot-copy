# Renders the home page.
class HomeController < ApplicationController
  include Facebook::Messenger

  def index
  end

  def set_and_subscribe
    # CHATBOT PROFILE SETTING
    Facebook::Messenger::Profile.set({
      greeting: [
        {
          locale: 'default',
          text: "Hello {{user_first_name}}, nice to have you here. How can I help you?"
        }
      ],
      get_started: {
        payload: 'GET_STARTED_PAYLOAD'
      },
      whitelisted_domains:[
        "https://dentasked-chatbot-staging.gorated.com"
      ]
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])


    Facebook::Messenger::Profile.set({
      persistent_menu: [
        {
          locale: 'default',
          composer_input_disabled: false,
          call_to_actions: [
            {
              title: 'Fun Facts about Oral Health',
              type: 'postback',
              payload: 'ORAL_HEALTH_PAYLOAD'
            },
            {
              title: "Tips For Best Oral Hygiene",
              type: 'postback',
              payload: 'ORAL_HYGIENE_PAYLOAD'
            },
            {
              title: 'Learn More',
              type: 'nested',
              call_to_actions: [
                {
                  title: "Schedule Dental Appointment",
                  type: 'postback',
                  payload: 'SCHEDULE_PAYLOAD'
                },
                {
                  title: "Subscribe",
                  type: 'postback',
                  payload: 'SUBSCRIBE_PAYLOAD'
                }
              ]
            },
          ]
        }
      ]
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])


    # CHATBOT PROFILE UNSETTING
    # Facebook::Messenger::Profile.unset({
    #   fields: [
    #         "greeting",
    #         "get_started",
    #         "persistent_menu"
    #       ]
    # }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])

    Facebook::Messenger::Subscriptions.subscribe(access_token: ENV['FB_PAGE_ACCESS_TOKEN'])
  end
end
