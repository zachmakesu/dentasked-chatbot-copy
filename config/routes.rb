Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq" # monitoring console
  mount Facebook::Messenger::Server, at: 'bot'
  root "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "/home/set_and_subscribe",           to: "home#set_and_subscribe"
end
